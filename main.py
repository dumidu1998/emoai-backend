import os
import sys
import threading
import json
import requests
from datetime import datetime
import time
sys.path.insert(0, '/utils/')
sys.path.insert(0, '/reinforcement_learning/')

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  

import flask
from flask import request, jsonify
from flask_cors import CORS, cross_origin

import firebase_admin
from firebase_admin import credentials, storage, firestore

from moviepy.video.io.VideoFileClip import VideoFileClip
import av

# from keras.models import load_model
from tensorflow.keras.models import load_model
import imageio.v3 as iio
import numpy as np
import cv2

import librosa
from scipy.stats import zscore


import speech_recognition as sr
from transformers import AutoTokenizer, TFAutoModel, TFAutoModelForSeq2SeqLM, TFAutoModelWithLMHead

from transformers import TextClassificationPipeline
from tensorflow.keras.applications.vgg16 import preprocess_input,decode_predictions

# custom imports
from utils.mediapipe_utils import detect_face_and_crop, grayscale_conversion
from utils.f1_score import f1_score
from utils.handlers import handleLabel
from utils.firebase_utils import generate_signed_url_from_blob
from utils.spectrogram_utils import mel_spectrogram, frame, chunker
from utils.image_utils import save_image
from utils.firestore_utils import save_data, update_data, get_all_data_in_collection, get_data_in_document, get_token_of_user, save_double_collection_data
from utils.firestore_utils import get_user_details, update_double_collection_data, get_default_weights, update_activity_status, update_reward, get_weights, save_new_weights, get_default_adding_constant, get_double_collection_data, change_to_new_emotion, get_default_case_3_dividing_factor
from utils.firestore_utils import get_exploration_rate, save_new_exploration_rate, increment_total, increment_error, increment_success
from utils.fcm_utils import send_fcm_notification
from reinforcement_learning.reinforcement_learning_main import reward_scaling, exploration_decay, suggest_activity, suggest_random_activity, save_activity_on_db, update_q_value


app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/vtest', methods=['POST','GET'])
@cross_origin()
def main_fn_test():
    file = ''
    userEmail = ''
    time = ''
    if request.method == 'GET':
        userEmail = 'dumi@gmail.com'
        time = "2022-04-01T12:30:00"
        file = "files/kamal@gmail.com/What NOT to Say to a Depressed Loved One_Trim.mp4"
    else:
        userEmail = request.json.get('userEmail', None)
        time = request.json.get('time', None).split(".", 1)[0] if "." in request.json.get('time', None) else request.json.get('time', None)
        file = request.json.get('file', None)
    return background_taskWithFileToken(userEmail, time, file)


@app.route('/uploadvideo', methods=['POST'])
@cross_origin()
def main_fn():
    file = ''
    userEmail = ''
    time = ''
    
    userEmail = request.json.get('userEmail', None)
    time = request.json.get('time', None).split(".", 1)[0] if "." in request.json.get('time', None) else request.json.get('time', None)
    file = request.json.get('file', None)
    return background_taskWithFileToken(userEmail, time, file)


def background_taskWithFileToken(userEmail, time, file):
    db = None
    if not firebase_admin._apps:
        cred = credentials.Certificate('uniapppro-4a047-firebase-adminsdk-wnpad-c6430e288c.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'uniapppro-4a047.appspot.com'
        })

    db = firestore.client()

    # from urllib.parse import unquote
    # url = unquote(file)

    url = file

    videoclip = VideoFileClip(url) 
    audioclip = videoclip.audio 
    # print(audioclip.to_soundarray()) 
    audioclip.write_audiofile("audio.wav") #TODO uncomment


    # container = av.open(url)
    # audio_stream = container.streams.audio[0]
    weights_dict = get_default_weights(db)
    VIDEO_CONST = weights_dict.get('facial')
    VOICE_CONST = weights_dict.get('voice')
    TEXT_CONST = weights_dict.get('text')
    transcript=''

    try:
        face_out=facial_emotion_recognition(url)
    except:
        face_out = {'Angry': 0.0, 'Disgust': 0.0, 'Fear': 0.0, 'Happy': 0.0, 'Neutral': 0.0, 'Sad': 0.0,  'Surprised': 0.0}
        VIDEO_CONST=0
    try:
        voice_out = voice_emotion_recognition()
    except:
        voice_out = {'Angry': 0.0, 'Disgust': 0.0, 'Fear': 0.0, 'Happy': 0.0, 'Neutral': 0.0, 'Sad': 0.0,  'Surprised': 0.0}
        VOICE_CONST=0
    try:
        transcript, text_out = text_emotion_recognition_from_API()
    except:
        TEXT_CONST=0
        text_out = "Neutral"

    # consider doing this??? 
    # voice_out['Angry'] += voice_out['Disgust']
    # face_out['Angry'] += face_out['Disgust']

    # consider doing this??? 
    # voice_out['Happy'] += voice_out['Surprised']
    # face_out['Happy'] += face_out['Surprised']


    # voice_out['Angry'] = (voice_out['Disgust'] + voice_out['Angry'])/2
    # face_out['Angry'] = (face_out['Disgust'] + face_out['Angry'])/2

    # voice_out['Happy'] = (voice_out['Surprised'] + voice_out['Happy'])/2
    # face_out['Happy'] = (face_out['Surprised'] + face_out['Happy'])/2

    
    if sum(voice_out.values()) != 0: 
        voice_out['Angry'] = round(((voice_out['Disgust']/100 + voice_out['Angry']/100)/(sum(voice_out.values())/100))*100,2)
   
    if sum(face_out.values()) != 0: 
        face_out['Angry'] =  round(((face_out['Disgust']/100 + face_out['Angry']/100)/(sum(face_out.values())/100))*100,2)

    if sum(voice_out.values()) != 0: 
        voice_out['Happy'] = round(((voice_out['Surprised']/100 + voice_out['Happy']/100)/(sum(voice_out.values())/100))*100,2)
   
    if sum(face_out.values()) != 0: 
        face_out['Happy'] =  round(((face_out['Surprised']/100 + face_out['Happy']/100)/(sum(face_out.values())/100))*100,2)
    
    del voice_out['Disgust']
    del face_out['Disgust']

    del voice_out['Surprised']
    del face_out['Surprised']

    # voice_out = {'Angry': 38.889, 'Fear': 5.556, 'Happy': 0.0, 'Neutral': 5.556, 'Sad': 0.0, }
    print("voice emotion is")
    print(voice_out)

    multiplied_dict_voice_out = {key: round(value * VOICE_CONST,5) for key, value in voice_out.items()}

    print('multiplied_dict_voice_out')
    print(multiplied_dict_voice_out)

    print("Face emotion is") 
    print(face_out)
    #output - {'Angry': 0.0, 'Fear': 0.0, 'Happy': 0.0, 'Neutral': 0.0, 'Sad': 0.0 }
    multiplied_dict_face_out = {key: round(value * VIDEO_CONST,5) for key, value in face_out.items()}

    print("multiplied_dict_face_out") 
    print(multiplied_dict_face_out)
    print("Text emotion is")
    print(text_out)

    # calculate weighted average val
    max_key_voice_out = max(multiplied_dict_voice_out, key=multiplied_dict_voice_out.get)
    max_value_voice_out = multiplied_dict_voice_out[max_key_voice_out]

    max_key_face_out = max(multiplied_dict_face_out, key=multiplied_dict_face_out.get)
    max_value_face_out = multiplied_dict_face_out[max_key_face_out]

    print("max key voice_out")
    print(max_key_voice_out)
    print(max_value_voice_out)

    print("max key face")
    print(max_key_face_out)
    print(max_value_face_out)

    finalLabel = [max_key_voice_out,max_key_face_out,text_out]
    finalOut = [max_value_voice_out,max_value_face_out,TEXT_CONST * 100]

    print("Final out [voice, face, text]")
    print(finalOut)

    max_index = np.argmax(finalOut)

    print("Final label [voice, face, text]")
    print(finalLabel)

    modelity = ''
    final_emotion = finalLabel[max_index]

    print("Final emotion index")
    print(max_index)
    print("Final emotion")
    print(final_emotion)

    if max_index == 0:
        print("voice")
        modelity = "voice"
    elif max_index == 1:
        print("face")
        modelity = "facial"
    else:
        print("text")
        modelity = "text"
    
    saved_body = {
            'max-modelity': modelity,
            'transcript-text': transcript,
            'email': userEmail,
            'time': datetime.fromtimestamp(datetime.strptime(time, "%Y-%m-%dT%H:%M:%S").timestamp()),
            'emotion': final_emotion,
            'text-emotion': text_out,
            'text-weight': round(TEXT_CONST * 100, 5),
            'current-weights': weights_dict, 
            'multiplied-weights': {
                'facial': multiplied_dict_face_out,
                'voice':  multiplied_dict_voice_out
            }
        }
    
    outt = save_double_collection_data(db, "emotions", userEmail, "emotions", saved_body)
    increment_total(db, userEmail)

    token = get_token_of_user(db, userEmail)
    send_fcm_notification(token, "Your new prediction is available!", final_emotion, {"emotionDocId":outt.id, "userEmail": userEmail})

    return jsonify(
        status='success',
        emotionDocId=outt.id,
        userEmail = userEmail,
        saved_body = saved_body
    )



'''POST https://duminodemailer-z2vh3qxhxq-as.a.run.app/agree
    {
    "emotionDocId": "1rp9HF4lIrEtnDwPto5L",
    "userEmail": "dumi@gmail.com"
    }
'''
@app.route('/agree', methods=['POST']) 
@cross_origin()
def agreePrediction():
    db = None
    if not firebase_admin._apps:
        cred = credentials.Certificate('uniapppro-4a047-firebase-adminsdk-wnpad-c6430e288c.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'uniapppro-4a047.appspot.com'
        })

    db = firestore.client()
    userEmail = request.json.get('userEmail', None)
    emotionId = request.json.get('emotionDocId', None)

    emotionData = get_double_collection_data(db, 'emotions', userEmail, 'emotions', emotionId)
    modelity = emotionData.get('max-modelity')
    k = get_default_adding_constant(db)

    weights=get_weights(db,userEmail)
    if(modelity == 'text'):
        weights[modelity] = weights[modelity] + k/10
    else:
        weights[modelity] = weights[modelity] + k

    save_new_weights(db, userEmail, weights.get('facial'), weights.get('voice'), weights.get('text'))
    increment_success(db, userEmail)
    return suggestActivity(userEmail, emotionId, emotionData.get('time'))

@app.route('/disagree', methods=['POST']) 
@cross_origin()
def disAgreePrediction():
    db = None
    if not firebase_admin._apps:
        cred = credentials.Certificate('uniapppro-4a047-firebase-adminsdk-wnpad-c6430e288c.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'uniapppro-4a047.appspot.com'
        })

    db = firestore.client()
    userEmail = request.json.get('userEmail', None)
    emotionId = request.json.get('emotionDocId', None)
    newEmotion = request.json.get('newEmotion', None)

    labels = ['Angry', 'Fear', 'Happy', 'Neutral', 'Sad']

    # Handle if sent with lower case
    for text in labels:
        if text.lower() == newEmotion.lower():
            newEmotion = text
            break

    emotionData = get_double_collection_data(db, 'emotions', userEmail, 'emotions', emotionId)
    increment_error(db, userEmail)

    if emotionData.get('emotion').lower() == newEmotion.lower(): 
        return jsonify(status='success!Already there!')

    # Update emotion in db 
    change_to_new_emotion(db, userEmail, emotionId, newEmotion) 

    modelity = emotionData.get('max-modelity')
    weights = emotionData.get('multiplied-weights')

    facialHighest = max(weights['facial'], key=weights['facial'].get)
    voiceHighest = max(weights['voice'], key=weights['voice'].get)
    text_emotion = emotionData.get('text-emotion')
    
    k = get_default_adding_constant(db)
    user_weights=get_weights(db,userEmail)
    case_3_dividing_factor=get_default_case_3_dividing_factor(db)

    if newEmotion.lower()==facialHighest.lower() or newEmotion.lower()==voiceHighest.lower():
        # Handle case 1
        if newEmotion.lower()==facialHighest.lower():
            #facial emotion
            user_weights['facial'] = user_weights['facial'] + k
            save_new_weights(db, userEmail, user_weights['facial'], user_weights['voice'], user_weights['text'] )

        if newEmotion.lower()==voiceHighest.lower():
            #voice emotion
            user_weights['voice'] = user_weights['voice'] + k
            save_new_weights(db, userEmail, user_weights['facial'], user_weights['voice'], user_weights['text'] )

    elif newEmotion.lower()==text_emotion.lower():
            #text emotion
            user_weights['text'] = user_weights['text'] + k/10
            save_new_weights(db, userEmail, user_weights['facial'], user_weights['voice'], user_weights['text'] )
    else:
        # Handle case 2
        correspond_facial_emotion_weight = weights['facial'][newEmotion]
        correspond_voice_emotion_weight = weights['facial'][newEmotion]
        correspond_text_emotion_weight = emotionData.get('text-weight')

        max_weight = max(correspond_text_emotion_weight, correspond_voice_emotion_weight, correspond_facial_emotion_weight)

        if max_weight == correspond_facial_emotion_weight:
            # print("Text has the highest emotion weight")
            user_weights['facial'] = user_weights['facial'] + k/case_3_dividing_factor
            save_new_weights(db, userEmail, user_weights['facial'], user_weights['voice'], user_weights['text'] )

        elif max_weight == correspond_voice_emotion_weight:
            # print("Voice has the highest emotion weight")
            user_weights['voice'] = user_weights['voice'] + k/case_3_dividing_factor
            save_new_weights(db, userEmail, user_weights['facial'], user_weights['voice'], user_weights['text'] )

        elif max_weight == correspond_text_emotion_weight and text_emotion == newEmotion:
            # print("Facial has the highest emotion weight")
            user_weights['text'] = user_weights['text'] + k/(case_3_dividing_factor*10)
            save_new_weights(db, userEmail, user_weights['facial'], user_weights['voice'], user_weights['text'] )
            
    print("new weights saved!", user_weights['facial'], user_weights['voice'], user_weights['text'] )
    return suggestActivity(userEmail, emotionId, emotionData.get('time'))


@app.route('/noactivity', methods=['POST']) 
@cross_origin()
def noactivity():
    db = None
    if not firebase_admin._apps:
        cred = credentials.Certificate('uniapppro-4a047-firebase-adminsdk-wnpad-c6430e288c.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'uniapppro-4a047.appspot.com'
        })

    db = firestore.client()

    userEmail = request.json.get('userEmail', None)
    activityId = request.json.get('activityId', None)

    update_activity_status(db, userEmail, activityId, 0)

    return {"status": "done"}


@app.route('/activitysubmit', methods=['POST']) 
@cross_origin()
def activityfeedback():
    db = None
    if not firebase_admin._apps:
        cred = credentials.Certificate('uniapppro-4a047-firebase-adminsdk-wnpad-c6430e288c.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'uniapppro-4a047.appspot.com'
        })

    db = firestore.client()

    userEmail = request.json.get('userEmail', None)
    activityId = request.json.get('activityId', None)
    reward = request.json.get('rating', None)

    if isinstance(reward, str):
        reward = int(reward)

    update_activity_status(db, userEmail, activityId, 1)
    update_reward(db, userEmail, activityId, reward)

    user = get_user_details(db, userEmail)
    if user.get('random'):
        return {"status": "done", "isRandom": 1}
    else:
        activityData = get_double_collection_data(db, 'users', userEmail, 'activity', activityId)
        action = activityData.get('activity')
        state = activityData.get('emotion')

        next_state = state # for simplicity, assume the next state is the same as the current state
        update_q_value(state, action, reward*reward_scaling, next_state, db, userEmail)
        save_new_exploration_rate(db, userEmail, get_exploration_rate(db, userEmail)*exploration_decay)
        return {"status": "done", "isRandom": 0}
 
 
@app.route('/testactivity', methods=['POST', 'GET']) 
@cross_origin()   
def testSuggestActivity():
    if request.method == 'GET':
        time = "2022-04-01T12:30:00"
        time = datetime.fromtimestamp(datetime.strptime(time, "%Y-%m-%dT%H:%M:%S").timestamp())
        emotionDocId = '33YKi1BCPNta7i2O370P'
        return suggestActivity('dumiduraj@gmail.com', emotionDocId, time)
    else:
        userEmail = request.json.get('userEmail', None)
        emotionId = request.json.get('emotionDocId', None)
        time = "2022-04-01T12:30:00"
        time = datetime.fromtimestamp(datetime.strptime(time, "%Y-%m-%dT%H:%M:%S").timestamp())
        return suggestActivity(userEmail, emotionId, time)


def suggestActivity(userEmail, emotionDocId, time): 
    db = None
    if not firebase_admin._apps:
        cred = credentials.Certificate('uniapppro-4a047-firebase-adminsdk-wnpad-c6430e288c.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'uniapppro-4a047.appspot.com'
        })

    db = firestore.client()

    token = get_token_of_user(db, userEmail)

    currentEmotion = get_double_collection_data(db, 'emotions', userEmail, 'emotions', emotionDocId).get('emotion')

    if currentEmotion in list(['Happy','Neutral']):
        print("No activity needed!!")
        send_fcm_notification(token, "You are all good!!", "Keep it up!", {})
        return jsonify({'status':'success', "activity": "None"})
    else:
        user = get_user_details(db, userEmail)
        if user.get('random'):
            print('suggesting random activity')
            action = suggest_random_activity(currentEmotion)
            # save suggested activity 
            saved_activity_id = save_activity_on_db(db, userEmail, currentEmotion, action, time)
            update_double_collection_data(db, 'users', userEmail, 'activity', saved_activity_id, {"isRandom": 1})
            send_fcm_notification(token, "Let's do an activity!", "Click me and lets do an activity!", 
            {"activityid": saved_activity_id, "activity": action, "userEmail": userEmail, "emotion": currentEmotion }
            )
        else:
            print('suggesting activity through RL')
            action = suggest_activity(currentEmotion, db, userEmail)
            # save suggested activity 
            saved_activity_id = save_activity_on_db(db, userEmail, currentEmotion, action, time)
            update_double_collection_data(db, 'users', userEmail, 'activity', saved_activity_id, {"isRandom": 0})
            send_fcm_notification(token, "Let's do an activity!", "Click me and lets do an activity!", 
            {"activityid": saved_activity_id, "activity": action, "userEmail": userEmail, "emotion": currentEmotion }
            )
        return jsonify({'status':'success', "activity": action, "activityId": saved_activity_id })


def facial_emotion_recognition(url):

    text_list = ['Angry', 'Disgust', 'Fear', 'Happy', 'Neutral', 'Sad', 'Surprised']
    out =[0,0,0,0,0,0,0]
    model = load_model('models/model.h5',custom_objects={"f1_score": f1_score })

    video = cv2.VideoCapture(url)

    # Check if the video file is opened successfully
    if not video.isOpened():
        print("Error: Could not open the video file.")
        raise ("Error opening the video") 

    # Get the dimensions of the first frame
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Release the video file
    video.release()
    is_landscape = 0

    # Determine the video orientation
    if width > height:
        is_landscape =1
    else:
        is_landscape =0

    # iterate over large videos
    frames = enumerate(iio.imiter(url, plugin="pyav"))
    x_arr=[]
    i=0
    
    for idx,frame in frames:
        # count the no of frames
        # if i>5:
        #     break
            # print(x_arr.shape)
            # p = model.predict(x_arr)
            # print(p)
        
        i=i+1
        if i%7 != 0:
            continue
        # print(frame.shape, frame.dtype)
        
        # if need to rotate
        # frame = np.rot90(frame, 1) 

        # Determine the video orientation
        if is_landscape == 1:
            frame = np.rot90(frame, 0) 
        else:
            frame = np.rot90(frame, 1) 

        # save_image(f"imgout/frame{idx:03d}.jpg", frame)
        # iio.imwrite(f"framep{idx:03d}.jpg", frame) 

        try:

            cropped = detect_face_and_crop(frame)  
            if cropped is not None: 
                # print("cropped.shape", cropped.shape)

                # save_image(f"imgout/frame{idx:03d}.jpg", cropped)

                # histogram equalization
                # cropped =  cv2.equalizeHist(grayscale_conversion(cropped))
                # print("cropped.shape after hist eq", cropped.shape)

                # save_image(f"imgout/framehisteq{idx:03d}.jpg", cropped)
                
                resized_frame = cv2.resize(cropped, (48,48))
                
                # print(resized_frame.shape)
                # resized_frame=resized_frame.reshape(48,48,3)
                # print(resized_frame.shape)
    
                # cv2.imwrite(f'q{int(time.time())}.jpg', resized_frame)
                # iio.imwrite(f"frame{idx:03d}.jpg", resized_frame)
                
                # print(resized_frame.shape)
                resized_frame=np.expand_dims(resized_frame, axis=0)
                # print(resized_frame.shape)
                p = model.predict(resized_frame)

                # ind = np.argmax(p[0])
                # out[ind]=out[ind]+1
                # for index, predictedData in enumerate(p[0]):
                    # print(index, predictedData)
                #     out[index] = out[index] + predictedData
                for oo in p:
                    ind = np.argmax(oo)
                    out[ind]=out[ind]+1
                
                # Below will sum up confidences in each prediction (commented since we are using majority voting)
                # for index, confidence in enumerate(p[0]):
                #     out[index] = out[index] + confidence
                    
                # print(np.argmax(p))
                # print(text_list[np.argmax(p)])

                # print("total")
                # print(out)
                x_arr.append(resized_frame)
        except:
            continue
            # return {'Angry': 0.0, 'Disgust': 0.0, 'Fear': 0.0, 'Happy': 0.0, 'Neutral': 0.0, 'Sad': 0.0, 'Surprised': 0.0}

            

    # p = model.predict(x_arr)
    # print(p)
    # print(text_list[np.argmax(p)]) 
    # faceData = {label: value for label, value in zip(text_list, out)}
    faceData = {label: round((value / i ) * 100, 5) for label, value in zip(text_list, out)}

    #output - {'Angry': 0.0, 'Disgust': 0.0, 'Fear': 0.0, 'Happy': 0.0, 'Neutral': 0.0, 'Sad': 0.0, 'Surprised': 100.0}

    # return text_list[np.argmax(p)]
    print('faceData')
    print(faceData)
    return faceData


def voice_emotion_recognition():
    audio_labels = ['Angry', 'Disgust', 'Fear', 'Happy', 'Neutral', 'Sad', 'Surprised']
    out =[0,0,0,0,0,0,0]
    
    # Sample rate (16.0 kHz)
    sample_rate = 16000     
    # Max pad lenght (3.0 sec)
    max_pad_len = 49100

    win_ts = 128

    hop_ts = 64

    signal=[]

    y, sr = librosa.core.load('audio.wav', sr=sample_rate, offset=0.5)
    # y, sr = librosa.core.load('03-01-03-01-01-01-12.wav', sr=sample_rate, offset=0.5) #happy

    y, _ = librosa.effects.trim(y) #Trim leading and trailing silence from an audio signal.
    
    for chunk in chunker(y, max_pad_len):
        if len(chunk)==max_pad_len:
            # Z-normalization 
            y = zscore(chunk)
            signal.append(chunk)
    
    mel_spect = np.asarray(list(map(mel_spectrogram, signal)))

    X_train = frame(mel_spect, hop_ts, win_ts)

    X_train = X_train.reshape(X_train.shape[0], X_train.shape[1] , X_train.shape[2], X_train.shape[3], 1)

    model2 = load_model('models/[CNN-LSTM]M.h5')
    o = model2.predict(X_train)

    for oo in o:
        ind = np.argmax(oo)
        out[ind]=out[ind]+1

    voiceData = {label: round((value/sum(out))*100,5) for label, value in zip(audio_labels, out)}

    # output = {'Angry': 38.889, 'Disgust': 50, 'Fear': 5.556, 'Happy': 0.0, 'Neutral': 5.556, 'Sad': 0.0, 'Surprised': 0.0}

    # print(audio_labels[np.argmax(out)])

    return voiceData


def text_emotion_recognition():
    r = sr.Recognizer()
    with sr.AudioFile('audio.wav') as source:
        # listen for the data (load audio to memory)
        audio_data = r.record(source)
        # recognize (convert from speech to text)
        text = r.recognize_google(audio_data)


        tokenizer = AutoTokenizer.from_pretrained("mrm8488/t5-base-finetuned-emotion")
        model = TFAutoModelWithLMHead.from_pretrained("mrm8488/t5-base-finetuned-emotion", from_pt=True)

        input_ids = tokenizer.encode(text + '</s>', return_tensors='tf') 

        output = model.generate(input_ids=input_ids,max_length=2)

        
        
        dec = [tokenizer.decode(ids) for ids in output]

        label = dec[0]

        emotion=handleLabel(label.split(">")[1].strip())

        # print(emotion)

        # return jsonify(emotion=handleLabel(label.split(">")[1].strip()))
        # return handleLabel(label.split(">")[1].strip())
        return (text, emotion)


def text_emotion_recognition_from_API():
    r = sr.Recognizer()
    with sr.AudioFile('audio.wav') as source:
        # listen for the data (load audio to memory)
        audio_data = r.record(source)
        # recognize (convert from speech to text)
        text = r.recognize_google(audio_data)
        # print(text)

        url = 'https://api-inference.huggingface.co/models/mrm8488/t5-base-finetuned-emotion'
        headers = {'Content-Type': 'application/json', 'referer': 'https://huggingface.co/'}
        data = {"inputs": text}

        response = requests.post(url, headers=headers, data=json.dumps(data))

        count=0
        while response.status_code != 200:
            if(count>5):
                return text_emotion_recognition()
            print(f"Error: {response.status_code}, {response.text}")
            print("Retrying...")
            time.sleep(20)
            response = requests.post(url, headers=headers, data=json.dumps(data))
            count = count + 1

        if response.status_code == 200:
            result = response.json()
            emotion = result[0]['generated_text']
            final = handleLabel(emotion)
            return (text,final)

        else:
            print(f"Error: {response.status_code}, {response.text}")


@app.route('/testfcm', methods=['GET'])
@cross_origin()
def testfcm():
    db = None
    if not firebase_admin._apps:
        cred = credentials.Certificate('uniapppro-4a047-firebase-adminsdk-wnpad-c6430e288c.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'uniapppro-4a047.appspot.com'
        })

    db = firestore.client()
    # token = get_token_of_user(db, 'dumi@gmail.com')
    # send_fcm_notification(token, "Header", "body",{"test":1})
    # outt = save_double_collection_data(db, "emotions", "dumi@gmail.com", "test", {
    #     'modelity' : "modelity",
    #     'email': "userEmail",
    #     'time': datetime.fromtimestamp(datetime.strptime("2022-04-01T12:30:00", "%Y-%m-%dT%H:%M:%S").timestamp()),
    #     'emotion': "final_emotion"
    # } )
    # print(outt.id)
    save_new_weights(db,'kamal@gmail.com', 13, 16, 0.5)
    c=get_weights(db,'kamal@gmail.com')
    print(c)
    return jsonify(
        url='outt.id',
    ) 
    
@app.errorhandler(404)
def page_not_found(e):
    response = {
        "status": e.code,
        "message": e.description,
    }
    return jsonify(response), 404


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
