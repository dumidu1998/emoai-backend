## Update requirements.txt
`pip install pipreqs`
`pipreqs`  

## for dev  

`git lfs pull`
`pip install -r requirements.txt`
`pyenv install`
`python main.py`
or
`gunicorn --bind :5900 --workers 1 --threads 8 --timeout 0 --reload main:app`

## IF errors occured

ImportError: libGL.so.1: cannot open shared object file: No such file or directory

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y
`sudo apt-get update && sudo apt-get install ffmpeg libsm6 libxext6 -y`


## For every Gitpod run
`sudo apt-get update && sudo apt-get install ffmpeg libsm6 libxext6 -y && python main.py`
`gunicorn --bind :5900 --workers 1 --threads 8 --timeout 0 --reload main:app`


## Docker Build and push and deploy

`docker login -u dumidu1998 -p 0084272b-fc0a-4476-9daf-9c4e8ccfe64a`

`docker buildx build -f dockerfile -t dumidu1998/emoai:latest .`

`docker login -u dumidu1998 -p 0084272b-fc0a-4476-9daf-9c4e8ccfe64a && docker buildx build -f dockerfile -t dumidu1998/emoai:latest . && docker push dumidu1998/emoai:latest`

`docker login -u dumidu1998 -p 0084272b-fc0a-4476-9daf-9c4e8ccfe64a && docker buildx build -f dockerfile -t dumidu1998/emoai:latest . && docker push dumidu1998/emoai:latest && ../google-cloud-sdk/bin/gcloud auth activate-service-account --key-file=keyfile/keyfile.json && ../google-cloud-sdk/bin/gcloud run deploy emoai --quiet --platform managed --allow-unauthenticated --region asia-southeast1 --image docker.io/dumidu1998/emoai --project loperaint --memory 8G --concurrency 1 --timeout 900 --cpu 2 --max-instances 2`

#### version 1.0
`gcloud run deploy emoai --quiet --platform managed --allow-unauthenticated --region asia-southeast1 --image docker.io/dumidu1998/emoai:1.0 --project loperaint --memory 8G --concurrency 1 --timeout 900 --cpu 2`


`docker run -e PORT=8080 -p 8080:8080 dumidu1998/emoai`

`docker system prune ` -a

### Install gcloud CLI - Run on path - /workspace

`curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-420.0.0-linux-arm.tar.gz && tar -xf google-cloud-cli-420.0.0-linux-arm.tar.gz && ./google-cloud-sdk/install.sh` 

### Login to gcloud account
`gcloud auth activate-service-account --key-file=keyfile/keyfile.json`


## for dev  

`git lfs pull`
`pip install -r requirements.txt`
`pyenv install`
`python main.py`
or
`gunicorn --bind :5900 --workers 1 --threads 8 --timeout 0 --reload main:app`

## IF errors occured

ImportError: libGL.so.1: cannot open shared object file: No such file or directory

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y
`sudo apt-get update && sudo apt-get install ffmpeg libsm6 libxext6 -y`


## For every Gitpod run
`sudo apt-get update && sudo apt-get install ffmpeg libsm6 libxext6 -y && python main.py`
`gunicorn --bind :5900 --workers 1 --threads 8 --timeout 0 --reload main:app`


## dummy request for send video emotion prediction
POST https://duminodemailer-z2vh3qxhxq-as.a.run.app/emologvideo

{
  "file": "path/to/file",
  "useremail": "dumi@gmail.com",
  "token": "FCM_TOKEN"
}


## dummy request for send activity suggestion
POST https://duminodemailer-z2vh3qxhxq-as.a.run.app/sendactivitynotification
{
token: "FCM_TOKEN"
}

## code for access bucket 
'''
  bucket = storage.bucket()
  <!-- file - relative path -->
  blob = bucket.blob(file)
  url = generate_signed_url_from_blob(blob)

  # Saving a file  
  # newBlob = bucket.blob("files/dumi.wav")
  # url = generate_signed_url_from_blob(newBlob)
  # outfile='requirements.txt'
  # newBlob.upload_from_filename(outfile)
'''