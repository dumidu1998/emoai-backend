import random

from reinforcement_learning.activities import emotion_wise_activities
from utils.firestore_utils import save_new_q_table, get_q_table, save_double_collection_data, get_exploration_rate


# Define hyperparameters
learning_rate = 0.7
discount_factor = 0.9
# initial_exploration_rate = 0.8  // defined in firestore utils
exploration_decay = 0.995
reward_scaling = 2.0

# Define a function to get the Q-value for a given state-action pair
def get_q_value(state, action, db, userEmail):
    q_table = get_q_table(db, userEmail)
    if state not in q_table:
        q_table[state] = {}
    if action not in q_table[state]:
        q_table[state][action] = 0.0
    return q_table[state][action]

# Define a function to update the Q-value for a given state-action pair
def update_q_value(state, action, reward, next_state, db, userEmail):
    q_table = get_q_table(db, userEmail)
    if state not in q_table:
        q_table[state] = {}
    if action not in q_table[state]:
        q_table[state][action] = 0.0
    old_q_value = get_q_value(state, action, db, userEmail)
    next_max_q_value = max([get_q_value(next_state, next_action, db, userEmail) for next_action in emotion_wise_activities[next_state]])
    new_q_value = old_q_value + learning_rate * (reward + discount_factor * next_max_q_value - old_q_value)
    q_table[state][action] = new_q_value
    save_new_q_table(db, userEmail, q_table)

# Define a function to select an action based on the current state and exploration rate
def select_action(state, exploration_rate, db, userEmail):
    if random.random() < exploration_rate:
        return random.choice(emotion_wise_activities[state])
    else:
        q_values = [get_q_value(state, action, db, userEmail) for action in emotion_wise_activities[state]]
        max_q_value = max(q_values)
        best_actions = [action for action, q_value in zip(emotion_wise_activities[state], q_values) if q_value == max_q_value]
        return random.choice(best_actions)

# Define a function to suggest activities based on the user's emotional state
def suggest_activity(emotion, db, userEmail):
    action = select_action(emotion, get_exploration_rate(db, userEmail), db, userEmail)
    return action

# Define a function to provide a reward based on user feedback
def provide_reward():
    reward = float(request.form['feedback'])
    return reward


def save_activity_on_db(db, userEmail, emotion, activity, time):
    return save_double_collection_data(db, 'users', userEmail, 'activity', {
        'emotion': emotion,
        'activity': activity,
        'time': time
    } ).id


def suggest_random_activity(emotion):
    if emotion not in emotion_wise_activities:
        raise ValueError("Invalid emotion. Please enter 'Sad', 'Angry', or 'Fear'.")
    
    activities = emotion_wise_activities[emotion]
    return random.choice(activities)