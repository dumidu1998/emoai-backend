emotion_wise_activities = {
'Angry': [
'Take slow, deep breaths for 2-3 minutes. Breathe in slowly through your nose, hold your breath for a few seconds, \
and then breathe out slowly through your mouth.',

'Tense and then release your muscles one by one. Start with your feet and work your way up to your head.',

'Challenge negative thoughts by asking yourself if they are true and replacing them with more positive and rational thoughts.',

'Focus on your breath and the sensations in your body for a few minutes. When your mind wanders, gently bring your attention back\
to your breath.',

'Visualize a peaceful place, such as a beach or a forest. Imagine yourself there, taking in the sights, sounds, and smells.',

'Write down three things you are grateful for each day.',

'Reach out to a friend or family member for emotional support when feeling angry or stressed.',

'Engage in physical activity for 30 minutes, such as going for a walk or doing a workout video.',

'Take a break from a situation or person that is causing anger. Go for a walk or listen to calming music for a few minutes.',

'Practice expressing yourself in a direct and respectful manner in a role-playing scenario with a friend or family member.'
],
'Sad': [
'Write down or repeat to yourself positive statements, such as "I am worthy and deserving of love and happiness."',

'Write down three things you are grateful for each day.',

'Engage in activities that bring you pleasure and joy, such as taking a warm bath, watching a funny movie, or listening to your \
    favorite music.',

'Focus on your breath and the sensations in your body for a few minutes. When your mind wanders, gently bring your attention \
    back to your breath.',

'Engage in physical activity for 30 minutes, such as going for a walk or doing a workout video.',

'Reach out to a friend or family member for emotional support when feeling sad or lonely.',

'Tense and then release your muscles one by one. Start with your feet and work your way up to your head.',

'Take a break from a situation or person that is causing sadness. Go for a walk or listen to calming music for a few minutes.',

'Challenge negative thoughts by asking yourself if they are true and replacing them with more positive and rational thoughts.',

'Visualize a positive outcome or future scenario. Imagine yourself succeeding and feeling happy and fulfilled.'
],
'Fear': [
'Take a few deep breaths, inhaling for a count of four, holding for a count of four, and exhaling for a count of four. Repeat \
    this for a few minutes until you feel more relaxed.',

'Tense and release your muscles one by one, starting from your toes and moving up to your head. Hold the tension for a few \
    seconds before releasing it, and focus on the difference between the tense and relaxed state.',

'Practice basic yoga poses, such as child’s, downward dog, and tree poses. Focus on your breath and the sensations in your \
    body as you move through each pose.',

'Go for a walk, run, or bike ride. Engaging in physical activity can help reduce anxiety and boost mood.',

'Practice some basic tai chi movements, such as the cloud hands or brush knee and twist step. Focus on your breath and the flow \
    of movement.',
'Sit or lie down comfortably and focus on your breath. When your mind wanders, gently bring your attention back to your breath.',

'Use a biofeedback device, such as a heart rate monitor, to learn how to regulate your physiological responses to stress.',

'Give yourself a mini hand or foot massage, or ask a partner or friend to give you a shoulder rub',

'Try acupressure by applying pressure to specific points on your body, such as the center of your palm or the inside of your wrist',

'Spend some time drawing, coloring, or painting. Focus on the process of creating rather than the end product'
]}