from firebase_admin import firestore

def save_data(db, collection, data, document=None):
    collection_ref = db.collection(collection)
    if document is None:
        return collection_ref.add(data)
    else:  
        return collection_ref.document(str(document)).set(data)

def save_double_collection_data(db, collection1, docid1, collection2, data):
    
    collection_ref = db.collection(collection1).document(docid1).collection(collection2).document()
    collection_ref.set(data)
    if collection1 == 'emotions':
        db.collection(collection1).document(docid1).set({
            "updated": firestore.SERVER_TIMESTAMP,
        })
    return collection_ref

def update_double_collection_data(db, collection1, docid1, collection2,  docid2, data):
    doc_ref = db.collection(collection1).document(docid1).collection(collection2).document(docid2)
    doc_ref.update(data)

def get_double_collection_data(db, collection1, docid1, collection2, docid2):
    doc_ref = db.collection(collection1).document(docid1).collection(collection2).document(docid2)
    doc = doc_ref.get()
    if doc.exists:
        return doc.to_dict()
    else:
        print(u'No such document!' + collection1 + docid1 + collection2 + docid2)
        raise Exception("No such document!" + collection1 + docid1 + collection2 + docid2)

def change_to_new_emotion(db, docid1, docid2, newEmotion): 
    collection1='emotions'  
    collection2='emotions'
    current_emotion = get_double_collection_data(db, collection1, docid1, collection2, docid2).get('emotion')

    doc_ref = db.collection(collection1).document(docid1).collection(collection2).document(docid2)
    doc_ref.update({
        "emotion": newEmotion,
        "invalidEmotion": current_emotion
    })

def update_activity_status(db, userEmail, activityDocId, status): 
    collection1='users'  
    collection2='activity'

    doc_ref = db.collection(collection1).document(userEmail).collection(collection2).document(activityDocId)
    doc_ref.update({
        "done": status,
    })

def update_reward(db, userEmail, activityDocId, reward): 
    collection1='users'  
    collection2='activity'

    doc_ref = db.collection(collection1).document(userEmail).collection(collection2).document(activityDocId)
    doc_ref.update({
        "reward": reward,
    })

def update_data(db, collection, document, data):
    doc_ref = db.collection(collection).document(document)
    doc_ref.set(data)

def get_all_data_in_collection(db, collection):
    collection_ref = db.collection(collection)
    docs = collection_ref.stream()

    results = []
    for doc in docs:
        results.append(doc.to_dict())
    
    return results

def get_data_in_document(db, collection, document):
    doc_ref = db.collection(collection).document(document)

    doc = doc_ref.get()
    if doc.exists:
        # print(f'Document data: {doc.to_dict()}')
        return doc.to_dict()
    else:
        print(u'No such document!' + collection + document)
        raise Exception("No such document!")


def get_token_of_user(db, useremail, collection='users'):
    doc_ref = db.collection(str(collection)).document(str(useremail))

    doc = doc_ref.get()
    if doc.exists:
        return doc.to_dict().get('fcmToken')
    else:
        print(u'No such document!')
        raise Exception("No such document! Invalid user email")

def get_user_details(db, useremail, collection='users'):
    doc_ref = db.collection(str(collection)).document(str(useremail))

    doc = doc_ref.get()
    if doc.exists:
        return doc.to_dict()
    else:
        print(u'No such document!')
        raise Exception("No such document! Invalid user email")

def get_default(db, key):
    return get_data_in_document(db,'defaults',key)

def get_default_weights(db):
    out = get_default(db, 'weights')
    return {'facial': out.get('facial'), 'voice': out.get('voice'), 'text': out.get('text')}

def get_default_adding_constant(db):
    out = get_default(db, 'addingconstant')
    return out.get('k')

def get_default_case_3_dividing_factor(db):
    out = get_default(db, 'case-3-dividing-factor')
    return out.get('value')

def get_weights(db, userEmail):
    doc_ref = db.collection('users').document(userEmail)
    data = doc_ref.get().to_dict()

    if 'weights' in data:
        return {'facial': data.get('weights').get('facial'), 'voice': data.get('weights').get('voice'), 'text': data.get('weights').get('text')}
    else:
        return get_default_weights(db)

def save_new_weights(db, userEmail, f, v, t):
    doc_ref = db.collection('users').document(userEmail)
    doc_ref.update({
        'weights': {
            'facial': f,
            'voice': v,
            'text': t
        }
    })

def get_q_table(db, userEmail):
    out = get_data_in_document(db, 'users', userEmail)
    ret = out.get('qTable')
    if ret is None:
        return {}  
    return ret

def save_new_q_table(db, userEmail, qTable):
    doc_ref = db.collection('users').document(userEmail)
    doc_ref.update({
        'qTable': qTable
    })

def get_exploration_rate(db, userEmail):
    out = get_data_in_document(db, 'users', userEmail)
    ret = out.get('exploration_rate')
    if ret is None:
        # initial_exploration_rate = 0.8
        return 0.8 
    return ret

def save_new_exploration_rate(db, userEmail, new_value):
    doc_ref = db.collection('users').document(userEmail)
    doc_ref.update({
        'exploration_rate': new_value
    })

def increment_total(db, userEmail):
    doc_ref = db.collection('users').document(userEmail)
    doc_ref.update({
        'total_predictions': firestore.Increment(1)
    })

def increment_success(db, userEmail):
    doc_ref = db.collection('users').document(userEmail)
    doc_ref.update({
        'success_count': firestore.Increment(1)
    })

def increment_error(db, userEmail):
    doc_ref = db.collection('users').document(userEmail)
    doc_ref.update({
        'error_count': firestore.Increment(1)
    })