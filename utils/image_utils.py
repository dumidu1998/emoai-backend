import imageio.v3 as iio

def save_image(name_with_path, source):
    iio.imwrite(name_with_path, source)