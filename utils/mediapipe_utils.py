import mediapipe as mp
import cv2



def detect_face_and_crop(input_image):
    mp_face_detection = mp.solutions.face_detection
    mp_drawing = mp.solutions.drawing_utils 
    drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)

    with mp_face_detection.FaceDetection(min_detection_confidence=0.5, model_selection=0) as face_detection:
        # Convert the BGR image to RGB and process it with MediaPipe Face Detection.
        # print(input_image.dtype)
        input_image = cv2.normalize(input_image, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        results =""
        if is_gray(input_image):
            results = face_detection.process(input_image)
        else:
            results = face_detection.process(cv2.cvtColor(input_image, cv2.COLOR_BGR2RGB))

        if not results.detections:
            print("No face detected!!")
            raise Exception("No face detected!!")
            return
        if len(results.detections)>1:
            print("More than one face is detected! skipping!")
            return

        annotated_image = input_image.copy()

        #preview before annotation
        # resize_and_show(annotated_image)

        detection = results.detections[0]
        image_rows, image_cols, _ = annotated_image.shape

        #preview after annotation
        # mp_drawing.draw_detection(annotated_image, detection)
        # resize_and_show(annotated_image)

        try:
            relative_bounding_box = detection.location_data.relative_bounding_box
            rect_start_point = mp_drawing._normalized_to_pixel_coordinates(
                relative_bounding_box.xmin, relative_bounding_box.ymin, image_cols,
                image_rows)
            rect_end_point = mp_drawing._normalized_to_pixel_coordinates(
                relative_bounding_box.xmin + relative_bounding_box.width,
                relative_bounding_box.ymin + relative_bounding_box.height, image_cols,
                image_rows)
            xleft,ytop=rect_start_point
            xright,ybot=rect_end_point
            crop_img = annotated_image[ytop: ybot, xleft: xright]

            # save cropped image
            # cv2.imwrite(f'crop_{int(time.time())}.jpg', crop_img)
            # iio.imwrite(f'crop_{int(time.time())}.jpg', crop_img) 
            return crop_img
        except Exception as e:
            print(e)
            return annotated_image


def is_gray(img):
  if len(img.shape) < 3: return True
  if img.shape[2]  == 1: return True
  b,g,r = img[:,:,0], img[:,:,1], img[:,:,2]
  if (b==g).all() and (b==r).all(): return True
  return False


def grayscale_conversion(img):
  if is_gray(img):
    return img
  else:
    return cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)   