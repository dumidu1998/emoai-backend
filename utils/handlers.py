def handleLabel(str):
    # audio_labels = ['', 'Disgust', '', '', '', '', '']
    if str == "sadness":
        return "Sad"

    elif str == "joy":
        return "Happy"

    elif str == "love":
        return "Neutral"
        
    elif str == "anger":
        return "Angry"

    elif str == "fear":
        return "Fear"

    elif str == "surprise":
        return "Suprised"

