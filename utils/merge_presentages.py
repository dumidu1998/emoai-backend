percentage_1 = 52
percentage_2 = 21
percentage_3 = 27

# Convert to decimal form
decimal_1 = percentage_1 / 100
decimal_2 = percentage_2 / 100
decimal_3 = percentage_3 / 100

# Calculate the total sum
total_sum = decimal_1 + decimal_2 + decimal_3

# Calculate the merged percentage
merged_decimal = (decimal_1 + decimal_2) / total_sum
merged_percentage = round(merged_decimal * 100, 2)  # Round to 2 decimal places

print(f"Merged percentage: {merged_percentage}%")
