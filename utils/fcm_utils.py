import requests

def send_fcm_notification(token, title, body, data=None):
    url = 'https://fcm.googleapis.com/fcm/send'
    headers = {
        'Authorization': 'key=AAAANZx1ITQ:APA91bEHvvqjClwqrV_kPpe7ceH3cpmJmZMG8eLLwYh3cw6ibLLWl9T6pxu1yhdtkfmEQW_c3p1v8VyksDc-hWtvQRyCYxnbxWeUhUSNzXQkvolnTOV7k2DNJeJRmvcy7NtysCgikchG',
        'Content-Type': 'application/json'
    }
    data_dict = {
        'to': token,
        'notification': {
            'title': title,
            'body': body,
        }
    }
    if data is not None:
        data_dict['data'] = data
    response = requests.post(url, headers=headers, json=data_dict)
    if response.status_code == 200:
        print('FCM notification sent successfully')
    else:
        print('Error sending FCM notification: ', response.text)


"""
    send_fcm_notification(token, "Header", "body",{"test":1})

"""