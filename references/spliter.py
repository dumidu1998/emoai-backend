import cv2
import numpy as np
import moviepy.editor as mp

# num fo clips 
num_clips=10

# file name [mp4]
file_path="https://firebasestorage.googleapis.com/v0/b/uniapppro-4a047.appspot.com/o/files%2Fkamal%40gmail.com%2F2023-01-04%2023%3A30%3A07.910402?alt=media&token=e903cfc8-de54-4dc4-ad54-98b417c91029"

# Open the video file
video = cv2.VideoCapture(file_path)

# Set the video codec
fourcc = cv2.VideoWriter_fourcc(*'mp4v')

# Set the frame rate and resolution
fps = video.get(cv2.CAP_PROP_FPS)
frame_width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

# Calculate the total number of frames in the video
total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

# Calculate the number of frames per clip
frames_per_clip = total_frames // num_clips

# Initialize the clip counter
clip_counter = 1

# Initialize the starting and ending frame numbers
start_frame = 0
end_frame = frames_per_clip

# Set the output file name
output_file_name = "clip_{}.mp4"

# Initialize the video write object
video_writer = cv2.VideoWriter(output_file_name.format(clip_counter), fourcc, fps, (frame_width, frame_height))

# Iterate over the frames
frame_counter=0
while video.isOpened():
    # Read a frame
    ret, frame = video.read()
    if ret:
        # Write the frame to the output video
        video_writer.write(frame)

        # Increment the frame counter
        frame_counter += 1

        # Check if we have reached the end of the current clip
        if frame_counter > end_frame:
            # Close the video write object
            video_writer.release()

            # Increment the clip counter
            clip_counter += 1

            # Set the starting and ending frame numbers for the next clip
            start_frame = end_frame + 1
            end_frame += frames_per_clip

            # Set the output file name for the next clip
            output_file_name = "clip_{}.mp4"

            # Initialize the video write object for the next clip
            video_writer = cv2.VideoWriter(output_file_name.format(clip_counter), fourcc, fps, (frame_width, frame_height))
    else:
        break

# Release the video capture and write objects
video.release()
video_writer.release()

# Extract the audio from the input video
clip = mp.VideoFileClip(file_path)
clip.audio.write_audiofile("audio.wav")

import wave
import math

# num of parts of the audio
num_parts=num_clips
# Open the input audio file
input_audio = wave.open("audio.wav", "rb")

# Get the audio parameters
num_channels = input_audio.getnchannels()
sample_width = input_audio.getsampwidth()
sample_rate = input_audio.getframerate()
num_frames = input_audio.getnframes()

# Calculate the length of the audio in seconds
audio_length = num_frames / float(sample_rate)

# Calculate the length of each part in seconds
part_length = audio_length / num_parts

# Calculate the number of frames in each part
part_frames = int(part_length * sample_rate)

# Initialize the part counter
part_counter = 1

# Initialize the starting and ending frame numbers
start_frame = 0
end_frame = part_frames

# Iterate over the parts
while start_frame < num_frames:
    # Open the output audio file
    output_audio = wave.open("part_{}.wav".format(part_counter), "wb")

    # Set the audio parameters
    output_audio.setnchannels(num_channels)
    output_audio.setsampwidth(sample_width)
    output_audio.setframerate(sample_rate)

    # Read the audio frames
    input_audio.setpos(start_frame)
    frames = input_audio.readframes(part_frames)

    # Write the audio frames to the output file
    output_audio.writeframes(frames)

    # Increment the part counter
    part_counter += 1

    # Set the starting and ending frame numbers for the next part
    start_frame = end_frame
    end_frame += part_frames

# Close the input and output audio files
input_audio.close()
output_audio.close()

print("Done!")